package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.api.repository.IRepository;
import ru.tsc.marfin.tm.enumerated.Sort;
import ru.tsc.marfin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);
}
