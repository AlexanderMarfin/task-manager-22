package ru.tsc.marfin.tm.api.repository;

import ru.tsc.marfin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M add(M model);

    M findOneByIndex(Integer index);

    M findOneById(String id);

    M remove(M model);

    M removeByIndex(Integer index);

    M removeById(String id);

    void removeAll(Collection<M> collection);

    void clear();

    boolean existsById(String id);

    long getSize();

}
