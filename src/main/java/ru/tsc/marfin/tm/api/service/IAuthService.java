package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

    User getUser();

    String getUserId();

    boolean isAuth();

    void checkRoles(Role[] roles);
}
