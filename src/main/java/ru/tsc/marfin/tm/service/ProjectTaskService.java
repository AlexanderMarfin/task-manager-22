package ru.tsc.marfin.tm.service;

import ru.tsc.marfin.tm.api.repository.IProjectRepository;
import ru.tsc.marfin.tm.api.repository.ITaskRepository;
import ru.tsc.marfin.tm.api.service.IProjectTaskService;
import ru.tsc.marfin.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.marfin.tm.exception.entity.TaskNotFoundException;
import ru.tsc.marfin.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.marfin.tm.exception.field.TaskIdEmptyException;
import ru.tsc.marfin.tm.exception.field.UserIdEmptyException;
import ru.tsc.marfin.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);;
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);;
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.stream()
                .forEach(task -> taskRepository.removeById(userId, task.getId()));
        projectRepository.removeById(userId, projectId);
    }

}
