package ru.tsc.marfin.tm.command.project;

import ru.tsc.marfin.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";

    public static final String DESCRIPTION = "Create new project";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER START DATE:");
        final Date dateStart = TerminalUtil.nextDate();
        System.out.println("ENTER END DATE:");
        final Date dateEnd = TerminalUtil.nextDate();
        final String userId = getUserId();
        getProjectService().create(userId, name, description, dateStart, dateEnd);
    }

}
