package ru.tsc.marfin.tm.exception.system;

import ru.tsc.marfin.tm.exception.AbstractException;

public class AuthorisationDeniedException extends AbstractException {

    public AuthorisationDeniedException() {
        super("Error! Login or password is incorrect...");
    }

}
